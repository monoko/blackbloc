use serde::{Deserialize, Serialize};
use reqwest::Error;

#[derive(Debug, Serialize, Deserialize)]
pub struct Response {
  pub status: Option<String>,
  pub message: Option<String>,
  pub content: serde_json::Value,
  pub error: Option<String>,
}

#[tokio::main]
pub async fn send<T: Serialize>(endpoint: String, path: String, req: T) -> Result<Response, Error> {
  let res = reqwest::Client::new()
      .post(format!("{}/api/{}", endpoint, path))
      .json(&req)
      .send()
      .await?;

  let user: Response = res.json::<Response>().await?;
  println!("{:?}", user);

  Ok(user)
}
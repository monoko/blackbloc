mod cmds; // Import commands

use clap::{Parser, Subcommand};

#[derive(Debug, Parser)]
#[clap(name = "blackbloc")]
#[clap(about = "A simple and safe client for bloc", long_about = None)]
struct Cli {
    #[clap(subcommand)]
    command: Commands,

    /// Server endpoint
    #[clap(short, long, required = true)]
    endpoint: String,
}

#[derive(Debug, Subcommand)]
enum Commands {
    Login(cmds::login::Login),
    Register(cmds::register::Register),
}

fn main() {
    let args = Cli::parse();

    match args.command {
        // Login to an account
        Commands::Login(cmds::login::Login{ username, password }) => {
            cmds::login::login(username, password);
        }

        // Register a new account
        Commands::Register(cmds::register::Register{ username, password }) => {
            cmds::register::register(args.endpoint, username, password);
      }
    }
}
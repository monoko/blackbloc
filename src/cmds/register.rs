#[path="../api/mod.rs"] mod api;

use clap::{Parser};
use serde::Serialize;

#[derive(Debug, Parser)]
#[clap(arg_required_else_help = true)]
pub struct Register {
    /// Username
    #[clap(short, long, required = false)]
    pub username: String,

    /// Password
    #[clap(short, long, required = false)]
    pub password: String,
}

#[derive(Debug, Serialize)]
pub struct Request {
  pub username: String,
  pub password: String,
  pub private_key: String,
  pub public_key: String,
}

pub fn register(endpoint: String, username: String, password: String) {
  let res = api::request::send(endpoint, String::from("auth/register"), Request {
    username: username,
    password: password,
    private_key: String::from("pbkeypbkeypbkeypbkeypbkeypbkeypbkeypbkeypbkeypbkeypbkeypbkey"),
    public_key: String::from("pvkeypvkeypvkeypvkeypvkeypvkeypvkeypvkeypvkeypvkeypvkeypvkeypvkey"),
  });

  match res {
    Ok(r) => {
      println!("yay {:?}", r);
    }

    Err(e) => {
      panic!("Erroor :c - {:?}", e)
    }
  }
}
use clap::{Parser};

#[derive(Debug, Parser)]
#[clap(arg_required_else_help = true)]
pub struct Login {
    /// Username
    #[clap(short, long, required = false)]
    pub username: String,

    /// Password
    #[clap(short, long, required = false)]
    pub password: String,
}

pub fn login(username: String, password: String) {
  println!("Logging in {} {}", username, password)
}